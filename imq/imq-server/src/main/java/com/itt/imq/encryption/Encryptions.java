package com.itt.imq.encryption;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class Encryptions 
{
	static String password="password";
	static byte[] salt=new String("12345678").getBytes();
	static int iterationCount = 40000;
	static int keyLength = 128;
	
    public static String encrypt(String inputString) throws Exception 
    {
        SecretKeySpec key = createSecretKey(password.toCharArray(),salt, iterationCount, keyLength);
        String originalString=inputString;
        String encryptedString = StringEncryptor.encrypt(originalString, key);
        return encryptedString;
    }
    
    public static String decrypt(String inputString) throws Exception 
    {
         SecretKeySpec key = createSecretKey(password.toCharArray(),salt, iterationCount, keyLength);
         String originalString=inputString;
         String decryptedString = StringDecryptor.decrypt(originalString, key);
         return decryptedString;
    }

    private static SecretKeySpec createSecretKey(char[] password, byte[] salt, int iterationCount, int keyLength) throws NoSuchAlgorithmException, InvalidKeySpecException 
    {
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
        PBEKeySpec keySpec = new PBEKeySpec(password, salt, iterationCount, keyLength);
        SecretKey keyTmp = keyFactory.generateSecret(keySpec);
        return new SecretKeySpec(keyTmp.getEncoded(), "AES");
    }
}
