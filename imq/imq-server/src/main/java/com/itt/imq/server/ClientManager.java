package com.itt.imq.server;

import java.io.DataInputStream;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.sql.Timestamp;
import java.util.Date;

import org.json.JSONObject;

import com.itt.imq.database.DatabaseManager;
import com.itt.imq.dto.ClientDTO;
import com.itt.imq.util.Constants;
import com.itt.imq.util.JSONManager;

public class ClientManager extends Thread
{
    final DataInputStream dis; 
    final DataOutputStream dos; 
    final Socket socket; 
    final int clientPort;
    final InetAddress inetAddress;
    
    public ClientManager(Socket socket) throws IOException  
    { 
        this.socket = socket; 
        this.dis = new DataInputStream(socket.getInputStream());
		this.dos = new DataOutputStream(socket.getOutputStream()); 
		this.clientPort = socket.getPort();
		this.inetAddress = socket.getInetAddress();
    } 
  
    @Override
    public void run()  
    { 
    	Date date = new Date();
    	String connectedTime = new Timestamp(date.getTime()).toString();
    	JSONObject json;
        String receivedJsonString; 
        String receivedMessage;
        String toreturn; 
        
        DatabaseManager databaseManager=new DatabaseManager();
        ClientDTO client= new ClientDTO();
        
        while (true)  
        { 
            try 
            { 
            	Thread.currentThread();
				Thread.sleep(1000);
                dos.writeUTF("\nServer : Tell me Mr.Client on port number : "+clientPort+" Inet : "+inetAddress); 
                  
                // receive the answer from client 
                receivedJsonString = dis.readUTF();

                receivedMessage = JSONManager.getMessageFromJson(receivedJsonString);
                
                prepareClient(client,receivedMessage,connectedTime);
                
                databaseManager.addtoDB(client);
                
                //Echo Server
                toreturn = receivedMessage;
                json = JSONManager.getJSON(toreturn,clientPort,inetAddress,Constants.SERVER_PORT);
                
                System.out.println("Client on "+clientPort+" says "+receivedMessage);
                
                dos.writeUTF(json.toString()); 
                
                if(receivedMessage.equals("Exit") || receivedMessage.equals("Bye")) 
                {  
                	closeConnection(this.socket);
                	break;
                } 
            } 
            catch (IOException e) 
            { 
            	System.out.println("Client on "+clientPort+" disconnected abruptly");
            	prepareClient(client,"Unexpected Termination",connectedTime);
            	databaseManager.addtoDB(client);
            	Server.CURRENT_NO_OF_CLIENTS--;
            	System.out.println("Current number of clients : "+Server.CURRENT_NO_OF_CLIENTS);
                break;
            } 
            catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
        } 
          
        try 
        {
			this.dis.close();
			this.dos.close();
		} 
        catch (IOException e) 
        {
			e.printStackTrace();
		} 
    }

	private void closeConnection(Socket socket) throws IOException 
	{
        System.out.println("\nClient " + this.socket + " wants to close connection"); 
        System.out.println("Closing this connection."); 
        socket.close();
        Server.CURRENT_NO_OF_CLIENTS--;
        System.out.println("Current number of clients = "+Server.CURRENT_NO_OF_CLIENTS);
		System.out.println("Connection closed!\n");
	}
	
	private void prepareClient(ClientDTO client, String received, String connectedTime)
	{
		client.setClientId(inetAddress.toString().substring(1)+"P"+clientPort+"T"+connectedTime);
		client.setPortNumber(clientPort);
        client.setClientIP(inetAddress.toString().substring(1));
        client.setData(received);
        client.setConnectedTime(connectedTime);
	}
}
