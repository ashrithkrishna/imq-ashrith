package com.itt.imq.util;

public final class Constants {

	private Constants(){}
	
	public static final int SERVER_PORT=5056;
	
	public static final int MAX_NO_OF_CLIENTS = 2;
}
