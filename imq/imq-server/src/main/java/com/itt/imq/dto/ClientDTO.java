package com.itt.imq.dto;

public class ClientDTO 
{
	private int id;
	private String clientId;
	private int portNumber;
	private String clientIP;
	private String data;
	private String datasendTimeStamp;
	private String connectedTime;
	
	public String getConnectedTime() {
		return connectedTime;
	}
	public void setConnectedTime(String connectedTime) {
		this.connectedTime = connectedTime;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public int getPortNumber() {
		return portNumber;
	}
	public void setPortNumber(int portNumber) {
		this.portNumber = portNumber;
	}
	public String getClientIP() {
		return clientIP;
	}
	public void setClientIP(String clientIP) {
		this.clientIP = clientIP;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getDatasendTimeStamp() {
		return datasendTimeStamp;
	}
	public void setDatasendTimeStamp(String datasendTimeStamp) {
		this.datasendTimeStamp = datasendTimeStamp;
	}
}
