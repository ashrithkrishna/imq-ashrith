package com.itt.imq.database;

import java.sql.*;
import java.util.Date;

import com.itt.imq.dto.ClientDTO;
import com.itt.imq.encryption.Encryptions;

public class DatabaseManager 
{
	Connection connection;
	Statement statement;
	
	public DatabaseManager()
	{
		try 
		{
			connection=DriverManager.getConnection("jdbc:mysql://localhost:3306/imq-ashrith","root","root");  
			statement=connection.createStatement(); 
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}    
	}
	
	public void addtoDB(ClientDTO client)
	{
		Date date = new Date();
		PreparedStatement preparedStatement;
		
		try 
		{
			preparedStatement = connection.prepareStatement("insert into client(clientId,portNumber,clientIP,data,datasendTimeStamp,connectedTime) values(?,?,?,?,?,?)");
			preparedStatement.setString(1, client.getClientId());
			preparedStatement.setInt(2, client.getPortNumber());
			preparedStatement.setString(3, client.getClientIP());
			preparedStatement.setString(4, Encryptions.encrypt(client.getData()));
			preparedStatement.setString(5, new Timestamp(date.getTime()).toString());
			preparedStatement.setString(6, client.getConnectedTime());
			
			preparedStatement.executeUpdate();
		} 
		catch (SQLException e) {
			e.printStackTrace();
			
		} catch (Exception e) {
			e.printStackTrace();
		}  
	}
}
