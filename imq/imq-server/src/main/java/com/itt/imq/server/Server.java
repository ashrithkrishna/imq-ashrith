package com.itt.imq.server;

import java.net.*;

import com.itt.imq.util.Constants;

import java.io.*; 

public class Server 
{ 
	public static int CURRENT_NO_OF_CLIENTS=0;
	public static boolean wait=false;
	
	public static void main(String[] args) throws IOException  
    { 
        @SuppressWarnings("resource")
		ServerSocket serverSocket = new ServerSocket(Constants.SERVER_PORT);
        
        System.out.println("Server Started. Listening on port 5056.");
        
        while (true)  
        { 
            Socket socket = null; 	// socket object to receive incoming client requests 
            try 
            { 
            	if(Server.CURRENT_NO_OF_CLIENTS < Constants.MAX_NO_OF_CLIENTS)
            	{
            		socket = serverSocket.accept(); 
                    Server.CURRENT_NO_OF_CLIENTS++;

                    System.out.println("\nA new client is connected : " + socket); 
                    System.out.println("Current number of clients = " + Server.CURRENT_NO_OF_CLIENTS);
                    
                    // create a new thread object 
                    Thread thread = new ClientManager(socket); 
                    thread.start();   
            	}
            	else
            	{
            		Thread.currentThread();
					Thread.sleep(2000);
					continue;
            	}
            } 
            catch (Exception exception)
            { 
            	socket.close(); 
            	exception.printStackTrace(); 
            } 
        } 
    } 
} 
