import java.io.*; 
import java.net.*;
import java.util.Scanner;

import org.json.JSONObject;

import com.itt.imq.util.JSONManager; 

public class Client 
{ 
	public static void main(String[] args) throws IOException 
	{ 
		try
		{ 
			Scanner scanner = new Scanner(System.in); 
			
			// getting localhost ip 
			InetAddress ip = InetAddress.getByName("localhost"); 
			
			System.out.println("Connecting to server...");
			// establish the connection with server port 5056 
			Socket socket = new Socket(ip, 5056); 

			if(socket.isConnected()==true)
			{
				// obtaining input and out streams 
				DataInputStream dis = new DataInputStream(socket.getInputStream()); 
				DataOutputStream dos = new DataOutputStream(socket.getOutputStream()); 
				
				// the following loop performs the exchange of information between client and clientManager
				while (true) 
				{ 
					System.out.println(dis.readUTF()); 
					String tosendMessage = scanner.nextLine(); 
					
					JSONObject json = JSONManager.getJSON(tosendMessage);
					dos.writeUTF(json.toString()); 
					
					String received = dis.readUTF(); 
					System.out.println("Server says : "+received); 
					
					if(tosendMessage.equals("Exit") || tosendMessage.equals("Bye")) 
					{ 
						closeConnection(socket);
						break; 
					} 
				} 
				
				scanner.close(); 
				dis.close(); 
				dos.close(); 
			}
		}
		catch(ConnectException e)
		{ 
			System.out.println("Problem in connecting to the server. Please try after sometime.");
		} 
	}

	private static void closeConnection(Socket socket)
	{
		System.out.println("Closing this connection : " + socket); 
		try 
		{
			socket.close();
			System.out.println("Connection closed"); 
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		} 
	} 
} 
