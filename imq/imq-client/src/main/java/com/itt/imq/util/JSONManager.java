package com.itt.imq.util;

import java.net.InetAddress;
import java.sql.Timestamp;
import java.util.Date;

import org.json.JSONObject;

public class JSONManager 
{
	static Date date = new Date();
	
	public static JSONObject getJSON(String message)
	{
		JSONObject json = new JSONObject();
		json.put("message", message);
		json.put("timestamp", new Timestamp(date.getTime()).toString());
		
		System.out.println(json);
		
		return json;
	}
	
	public static String getMessageFromJson(String receivedJsonString)
	{
		JSONObject jsonObj =new JSONObject(receivedJsonString);
        
        return (String) jsonObj.get("message").toString();
	}

	public static JSONObject getJSON(String message, int clientPort, InetAddress inetAddress, int serverPort) 
	{
		JSONObject json = new JSONObject();
		json.put("message", message);
		json.put("timestamp", new Timestamp(date.getTime()).toString());
		json.put("clientPort", clientPort);
		json.put("inetAddress", inetAddress);
		json.put("serverPort", serverPort);
				
		System.out.println(json);
		
		return json;
	}
}
